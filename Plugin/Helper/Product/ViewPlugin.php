<?php
namespace TakeTheCake\AttributeSetLayoutHandle\Plugin\Helper\Product;

use Magento\Catalog\Helper\Product\View as Subject;
use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;

class ViewPlugin
{
    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * ViewPlugin constructor.
     *
     * @param DataObjectFactory               $dataObjectFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     */
    public function __construct(
        DataObjectFactory $dataObjectFactory,
        AttributeSetRepositoryInterface $attributeSetRepository
    ) {
        $this->dataObjectFactory      = $dataObjectFactory;
        $this->attributeSetRepository = $attributeSetRepository;
    }

    /**
     * Adds additional layout update handles based on product's attribute set:
     * - set_%attribute_set_name% (example: set_default)
     * - set_%attribute_set_name%_type_%product_type% (example: set_default_type_simple)
     * - set_%attribute_set_name%_id_%product_id% (example: set_default_id_1)
     * - set_%attribute_set_name%_sku_%product_sku% (example: set_default_sku_24-MB01)
     *
     * @param Subject    $subject
     * @param Page       $resultPage
     * @param Product    $product
     * @param DataObject $params
     *
     * @return array
     */
    public function beforeInitProductLayout(
        Subject $subject,
        Page $resultPage,
        $product,
        $params = null
    ) {
        $params       = is_null($params) ? $this->dataObjectFactory->create() : $params;
        $afterHandles = $params->getData('after_handles') ?: [];

        try {
            $afterHandles[] = $this->getAttributeSetCode($product);

            $params->setData('after_handles', $afterHandles);

        } catch (NoSuchEntityException $e) {
            // intentionally left empty since there is no reason neither to interrupt execution nor to handle the
            // exception in any way
        }

        return [$resultPage, $product, $params];
    }

    /**
     * Returns an attribute set "code" as lowercase name with spaces replaced to underscores
     *
     * @param Product $product
     *
     * @return string
     *
     * @throws NoSuchEntityException
     */
    private function getAttributeSetCode($product)
    {
        return 'set_' . strtr(
                strtolower($this->attributeSetRepository->get($product->getAttributeSetId())->getAttributeSetName()),
                ' ',
                '_'
            );
    }
}