<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'TakeTheCake_AttributeSetLayoutHandle',
    __DIR__
);