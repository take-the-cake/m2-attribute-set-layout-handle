Adds additional layout update handles based on product's attribute set:
- set_%attribute_set_name% (example: set_default)
- set_%attribute_set_name%\_type_%product_type% (example: set_default_type_simple)
- set_%attribute_set_name%\_id_%product_id% (example: set_default_id_1)
- set_%attribute_set_name%\_sku_%product_sku% (example: set_default_sku_24-MB01)